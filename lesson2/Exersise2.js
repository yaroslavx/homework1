console.log("Exersise #1")

const selectFromInterval = (arr, firstBorder, secondBorder) => {
  const e = new Error("Error!");
  if (isNaN(firstBorder) || isNaN(secondBorder) || arr.some(isNaN))
    throw new Error("Error!");
  return arr.filter(
    (el) =>
      (el <= firstBorder && el >= secondBorder) ||
      (el >= firstBorder && el <= secondBorder)
  );
};

console.log(selectFromInterval([1, 3, 5], 5, 2));
console.log(selectFromInterval([-2, -15, 0, 4], -13, -5));
console.log(selectFromInterval(["aaa"], 2, 3));

console.log("")