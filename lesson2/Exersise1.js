console.log("Exersise #1")

const makeObjectDeepCopy = (obj) => {
  if (obj === null) return null;
  let clone = Object.assign({}, obj);
  Object.keys(clone).forEach(
    (key) =>
      (clone[key] =
        typeof obj[key] === "object" ? makeObjectDeepCopy(obj[key]) : obj[key])
  );
  if (Array.isArray(obj)) {
    clone.length = obj.length;
    return Array.from(clone);
  }
  return clone;
};


// Example
const animals = {
  cats: { big: ["brown", "white"], small: ["black", "red"] },
  dogs: ["gray", "black"],
};
const animalsClone = makeObjectDeepCopy(animals);

console.log(animalsClone);

console.log("")